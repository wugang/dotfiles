;;; Header
;; -*- Mode: emacs-lisp -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

;;; Packages
;;;; Header

(defun dotspacemacs/layers ()
  "Configuration Layers declaration.
You should not put any user code in this function besides modifying the variable
values."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs
   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused
   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t
   ;; If non-nil layers with lazy install support are lazy installed.
   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()
   ;; List of configuration layers to load.

;;;; Config Layers

   dotspacemacs-configuration-layers
   '(
;;     rust
;;     vimscript
     ;; ----------------------------------------------------------------
     ;; Example of useful layers you may want to use right away.
     ;; Uncomment some layer names and press <SPC f e R> (Vim style) or
     ;; <M-m f e R> (Emacs style) to install them.
     ;; ----------------------------------------------------------------
     (auto-completion :variables
                      auto-completion-return-key-behavior 'complete
                      auto-completion-tab-key-behavior 'cycle
                      auto-completion-complete-with-key-sequence nil
                      auto-completion-complete-with-key-sequence-delay 0.1
                      auto-completion-private-snippets-directory nil)
     better-defaults
     bibtex
     chinese
     colors
     csv
     deft
     emacs-lisp
     ess
     games
     git
     graphviz
     helm
     html
     latex
     lua
     markdown
     mu4e
     org
     (shell :variables
            ;;http://spacemacs.org/layers/+tools/shell/README.html
            ;;default shell is eshell?
            shell-default-height 30
            shell-default-position 'bottom)
     pandoc
     pdf-tools
     plantuml
     python
     shell-scripts
     spell-checking
     syntax-checking
     ;;https://github.com/evacchi/tabbar-layer
     ;;tabbar
     version-control
     xkcd
     yaml
     )

;;;; Additional Packages

   dotspacemacs-additional-packages '(
                                      all-the-icons
                                      ;;https://github.com/domtronn/all-the-icons.el
                                      calfw
                                      calfw-org
                                      calfw-cal
                                      ;;https://github.com/kiwanami/emacs-calfw
                                      beacon
                                      ;;https://github.com/Malabarba/beacon
                                      circadian
                                      ;;https://github.com/GuidoSchmidt/circadian.el
                                      doom-themes
                                      ;;https://github.com/hlissner/emacs-doom-themes
                                      evil-goggles
                                      ;;https://github.com/edkolev/evil-goggles
                                      olivetti
                                      moe-theme
                                      ;;https://github.com/kuanyui/moe-theme.el#download
                                      minimap
                                      org-chef
                                      ;;https://github.com/Chobbes/org-chef
                                      org-dashboard
                                      ;;https://github.com/leoc/org-time-budgets
                                      ;;org-mind-map
                                      ;;https://github.com/theodorewiles/org-mind-map
                                      org-beautify-theme
                                      ;;https://github.com/jonnay/org-beautify-theme
                                      org-sticky-header
                                      ;;https://github.com/alphapapa/org-sticky-header
                                      org-super-agenda
                                      ;;https://github.com/alphapapa/org-super-agenda/
                                      org-time-budgets
                                      ;;https://github.com/leoc/org-time-budgets
                                      org-timeline
                                      ;;https://github.com/Fuco1/org-timeline
                                      org-noter
                                      org-ref
                                      ;;https://github.com/jkitchin/org-ref
                                      outshine
                                      ;;https://github.com/alphapapa/outshine
                                      popup
                                      ;;https://github.com/auto-complete/popup-el
                                      rainbow-delimiters
                                      ;;https://github.com/Fanael/rainbow-delimiters
                                      smtpmail
                                      use-package
                                      ;;visual-fill-column
                                      ;;https://github.com/joostkremers/visual-fill-column
                                      xresources-theme
                                      ;;https://github.com/username/repo
                                      ;;add packages from github
                                      ;;(PACKAGE-NAME
                                       ;;:location (recipe
                                                  ;;:fetcher github
                                                  ;;:repo "USERNAME/REPO")
                                       ;;)
                                      )
   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '()
   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages '(org-projectile
                                    evil-ediff
                                    exec-path-from-shell)
   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and uninstall any
   ;; unused packages as well as their unused dependencies.
   ;; `used-but-keep-unused' installs only the used packages but won't uninstall
   ;; them if they become unused. `all' installs *all* packages supported by
   ;; Spacemacs and never uninstall them. (default is `used-only')
   dotspacemacs-install-packages 'used-only))

;;; Config
;;;; Spacemacs Config
;;;;; General

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration.
You should not put any user code in there besides modifying the variable
values."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t
   ;; Maximum allowed time in seconds to contact an ELPA repository.
   dotspacemacs-elpa-timeout 5
   ;; If non nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil
   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'.
   dotspacemacs-elpa-subdirectory nil
   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'vim
   ;; If non nil output loading progress in `*Messages*' buffer. (default nil)
   dotspacemacs-verbose-loading nil
   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'official
   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `bookmarks' `projects' `agenda' `todos'."
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   dotspacemacs-startup-lists '((recents . 5)
                                (projects . 5))
   ;; True if the home buffer should respond to resize events.
   dotspacemacs-startup-buffer-responsive t
   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'org-mode
   ;; If non nil the cursor color matches the state color in GUI Emacs.
   dotspacemacs-colorize-cursor-according-to-state t
   ;; The leader key
   dotspacemacs-leader-key "SPC"
   ;; The key used for Emacs commands (M-x) (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"
   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"
   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"
   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","
   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m")
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"
   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs C-i, TAB and C-m, RET.
   ;; Setting it to a non-nil value, allows for separate commands under <C-i>
   ;; and TAB or <C-m> and RET.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil
   ;; If non nil `Y' is remapped to `y$' in Evil states. (default nil)
   dotspacemacs-remap-Y-to-y$ nil
   ;; If non-nil, the shift mappings `<' and `>' retain visual state if used
   ;; there. (default t)
   dotspacemacs-retain-visual-state-on-shift t
   ;; If non-nil, J and K move lines up and down when in visual mode.
   ;; (default nil)
   dotspacemacs-visual-line-move-text nil
   ;; If non nil, inverse the meaning of `g' in `:substitute' Evil ex-command.
   ;; (default nil)
   dotspacemacs-ex-substitute-global nil
   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"
   ;; If non nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil
   ;; If non nil then the last auto saved layouts are resume automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil
   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1
   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache
   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5
   ;; If non nil, `helm' will try to minimize the space it uses. (default nil)
   dotspacemacs-helm-resize nil
   ;; if non nil, the helm header is hidden when there is only one source.
   ;; (default nil)
   dotspacemacs-helm-no-header nil
   ;; define the position to display `helm', options are `bottom', `top',
   ;; `left', or `right'. (default 'bottom)
   dotspacemacs-helm-position 'bottom
   ;; Controls fuzzy matching in helm. If set to `always', force fuzzy matching
   ;; in all non-asynchronous sources. If set to `source', preserve individual
   ;; source settings. Else, disable fuzzy matching in all sources.
   ;; (default 'always)
   dotspacemacs-helm-use-fuzzy 'always
   ;; If non nil the paste micro-state is enabled. When enabled pressing `p`
   ;; several times cycle between the kill ring content. (default nil)
   dotspacemacs-enable-paste-transient-state nil
   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4
   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom
   ;; If non nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t
   ;; If non nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil
   ;; If non nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil
   ;; If non nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90
   ;; If non nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t
   ;; If non nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t
   ;; If non nil unicode symbols are displayed in the mode line. (default t)
   dotspacemacs-mode-line-unicode-symbols t
   ;; If non nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t
   ;; Control line numbers activation.
   ;; If set to `t' or `relative' line numbers are turned on in all `prog-mode' and
   ;; `text-mode' derivatives. If set to `relative', line numbers are relative.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; (default nil)
   dotspacemacs-line-numbers nil
   ;; Code folding method. Possible values are `evil' and `origami'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil
   ;; If non-nil smartparens-strict-mode will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil
   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc…
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil
   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all
   ;; If non nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil
   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `ag', `pt', `ack' and `grep'.
   ;; (default '("ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("ag" "pt" "ack" "grep")
   ;; The default package repository used if no explicit repository has been
   ;; specified with an installed package.
   ;; Not used for now. (default nil)
   dotspacemacs-default-package-repository nil
   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed'to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup nil

;;;;; Font

   ;; Default font, or prioritized list of fonts. `powerline-scale' allows to
   ;; quickly tweak the mode-line size to make separators look not too crappy.
   dotspacemacs-default-font '("Source Code Pro"
                               :size 18
                               :weight normal
                               :width normal
                               :powerline-scale 1.1)

;;;;; Theme

   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press <SPC> T n to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(spacemacs-dark
                         spacemacs-light)

   ))

;;;; User config
;;;;; Header

(defun dotspacemacs/user-init ()
  "Initialization function for user code.
It is called immediately after `dotspacemacs/init', before layer configuration
executes.
 This function is mostly useful for variables that need to be set
before packages are loaded. If you are unsure, you should try in setting them in
`dotspacemacs/user-config' first."
  )

(defun dotspacemacs/user-config ()
  "Configuration function for user code.
This function is called at the very end of Spacemacs initialization after
layers configuration.
This is the place where most of your configurations should be done. Unless it is
explicitly specified that a variable should be set before a package is loaded,
you should place your code here."

;;;;; General

  ;; Start emacs server if not already running
  (if (and (fboundp 'server-running-p)
           (not (server-running-p)))
      (server-start))

  ;;display time in mode line in 24 hour format
  (setq display-time-24hr-format t)
  (display-time-mode 1)

  ;;configuring longer delay for evil escape fd
  ;;https://github.com/syl20bnr/spacemacs/blob/develop/doc/DOCUMENTATION.org#escaping
  (setq-default evil-escape-delay 0.2)

  ;;activate use-package
  ;;for use in configuration
  ;;https://github.com/jwiegley/use-package
  ;;http://spacemacs.org/doc/LAYERS.html#use-package
  (require 'use-package)

  ;;enable adaptive-wrap
  ;;(use-package adaptive-wrap)
  ;;(setq 'adaptive-wrap-prefix-mode t)

  ;;powerline Separators
  (setq powerline-default-separator 'slant)

  ;;flyspell spell check
  (setq-default dotspacemacs-configuration-layers
                '((spell-checking :variables spell-checking-enable-auto-dictionary t)))

  ;;https://emacs.stackexchange.com/questions/13583/whats-the-difference-between-a-buffer-a-file-a-window-and-a-frame#13584
  ;;tend toward new frames/rather than windows when opening files/splitting
  ;;buffers
  ;;(setq pop-up-frames nil)

  ;;enable beacon minor mode
  (beacon-mode 1)

  ;;enable evil-goggles for visual hint when using evil
  ;; https://github.com/edkolev/evil-goggles
  (use-package evil-goggles
    :ensure t
    :config
    (evil-goggles-mode)

    ;; optionally use diff-mode's faces; as a result, deleted text
    ;; will be highlighed with `diff-removed` face which is typically
    ;; some red color (as defined by the color theme)
    ;; other faces such as `diff-added` will be used for other actions
    (evil-goggles-use-diff-faces))

  ;;scrolling
  ;;https://old.reddit.com/r/emacs/comments/8sw3r0/finally_scrolling_over_large_images_with_pixel/
  ;;Good speed and allow scrolling through large images (pixel-scroll). Note:
  ;;Scroll lags when point must be moved but increasing the number of lines that
  ;;point moves in pixel-scroll.el ruins large image scrolling. So unfortunately
  ;;I think we'll just have to live with this.
  (pixel-scroll-mode)
  (setq pixel-dead-time 0) ; Never go back to the old scrolling behaviour.
  (setq pixel-resolution-fine-flag t) ; Scroll by number of pixels instead of
                                      ; lines (t = frame-char-height pixels).
  (setq mouse-wheel-scroll-amount '(5)) ; Distance in pixel-resolution to scroll
                                        ; each mouse wheel event.
  (setq mouse-wheel-progressive-speed nil) ; Progressive speed is too fast for me.

;;;;; Color Highlighting

  ;;https://elpa.gnu.org/packages/rainbow-mode.html
  ;;http://www.totherme.org/configs/gds.html#orgheadline23
  ;;highlight hex color codes with color
  (use-package rainbow-mode
    :unless (string-prefix-p "example.com" (system-name))
    :diminish rainbow-mode
    :config
    (add-hook 'prog-mode-hook 'rainbow-mode)
    (add-hook 'emacs-lisp-mode-hook 'rainbow-mode)
    (add-hook 'css-mode-hook 'rainbow-mode)
    (add-hook 'html-mode-hook 'rainbow-mode)
    (add-hook 'LaTeX-mode-hook 'rainbow-mode)
    (add-hook 'ess-mode-hook 'rainbow-mode)
    (add-hook 'R-mode-hook 'rainbow-mode)
    (add-hook 'conf-mode-hook 'rainbow-mode)
    (add-hook 'conf-unix-mode-hook 'rainbow-mode))

  ;;https://github.com/Fanael/rainbow-delimiters
  ;;"rainbow parentheses"-like mode which highlights delimiters such as
  ;;parentheses, brackets or braces according to their depth.
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
  ;;https://github.com/Fanael/rainbow-identifiers
  ;;highlighting of identifiers based on their names. Each identifier gets a
  ;;color based on a hash of its name.
  (add-hook 'prog-mode-hook 'rainbow-identifiers-mode)

;;;;; Chinese


  ;;http://spacemacs.org/layers/+intl/chinese/README.html
  (setq-default dotspacemacs-configuration-layers '(
                                                    (chinese :variables chinese-default-input-method 'pyim)))

;;;;; Circadian

  ;;circadian configuration
  ;;https://www.reddit.com/r/spacemacs/comments/81jo5c/circadian_configuration_issues/
  (use-package circadian
    :ensure t
    :config
    (setq circadian-themes '(("8:00" . spacemacs-light)
                             ("18:30" . spacemacs-dark)))
    (circadian-setup))

;;;;; Org-mode

  ;;http://spacemacs.org/layers/+emacs/org/README.html

  (with-eval-after-load 'org

;;;;;; Org Directory, Agenda Files, & Default File

    ;;set location of agenda files to directory to source all .org files in it
    ;;https://stackoverflow.com/questions/11384516/how-to-make-all-org-files-under-a-folder-added-in-agenda-list-automatically

    (setq org-directory "~/Notes")
    (setq org-default-notes-file "~/Notes/Refile.org")
    (setq org-agenda-files '("~/Notes/"))

;;;;;; Aesthetics & Interface

    ;;Aesthetic configurations
    (setq org-ellipsis "  "
          ;;https://orgmode.org/manual/Special-symbols.html
          org-pretty-entities t
          ;;org-startup-indented t
          ;;org-startup-with-inline-images t
          ;;org-startup-with-latex-preview t
          org-fontify-quote-and-verse-blocks t
          ;;hide markup elements
          org-hide-emphasis-markers t
          ;;https://emacs.stackexchange.com/questions/14852/divider-lines-in-org-agenda)
          ;;add/remove horizontal divider lines in agenda
          org-agenda-block-separator ""
          ;;https://orgmode.org/manual/Clean-view.html#Clean-view
          ;;hide stars before first star of heading
          org-hide-leading-stars t
          ;;extend face to end of line
          org-fontify-whole-heading-line t
          ;;strike through done tasks
          ;;http://sachachua.com/blog/2012/12/emacs-strike-through-headlines-for-done-tasks-in-org/
          org-fontify-done-headline t
          org-fontify-quote-and-verse-blocks t
          spaceline-org-clock-p t
          ;;increase size of latex equation images
          ;;https://stackoverflow.com/questions/11272236/how-to-make-formule-bigger-in-org-mode-of-emacs
          org-format-latex-options (plist-put org-format-latex-options :scale 2.5))

    ;;emphasis configuration
    ;;https://emacs.stackexchange.com/questions/24361/org-emphasis-alist-add-additional-symbols
    ;;https://old.reddit.com/r/orgmode/comments/8n45ds/why_highlighting_text_is_so_painful_in_orgmode/
    (setq org-emphasis-alist
          (quote (("*" bold)
                  ("/" italic)
                  ("_" underline)
                  ("~" (:foreground "#212026" :background "#e0211d" ))
                  ("=" org-verbatim verbatim)
                  ("+" (:strike-through t))
                  )))

    ;;package displays in the header-line the
    ;;Org heading for the node that’s at the
    ;;top of the window. This way, if the
    ;;heading for the text at the top of the
    ;;window is beyond the top of the window
    (require 'org-sticky-header)
    ;; On org-mode startup, activate org-sticky-header
    (add-hook 'org-mode-hook 'org-sticky-header-mode)

;;;;;; Inline Image Thumbnail
    ;;configure org-inline image behavior//generate thumbnail for better image
    ;;width of many file types
    ;;https://stackoverflow.com/questions/15407485/inline-pdf-images-in-org-mode
    (setq image-file-name-extensions
          (quote
           ("png" "jpeg" "jpg" "gif" "tiff" "tif" "xbm" "xpm" "pbm" "pgm" "ppm" "pnm" "svg" "pdf" "bmp")))

    (setq org-image-actual-width 900)
    (setq org-imagemagick-display-command "convert -density 600 \"%s\" -thumbnail \"%sx%s>\" \"%s\"")
    (defun org-display-inline-images (&optional include-linked refresh beg end)
      ;;"Display inline images. Normally only links without a description are
      ;;inlined, because this is how it will work for export. When
      ;;INCLUDE-LINKED is set, also links with a description will be inlined.
      ;;This can be nice for a quick look at those images, but it does not
      ;;reflect what exported files will look like. When REFRESH is set, refresh
      ;;existing images between BEG and END. This will create new image displays
      ;;only if necessary. BEG and END default to the buffer boundaries."
      (interactive "P")
      (unless refresh
        (org-remove-inline-images)
        (if (fboundp 'clear-image-cache) (clear-image-cache)))
      (save-excursion
        (save-restriction
          (widen)
          (setq beg (or beg (point-min)) end (or end (point-max)))
          (goto-char beg)
          (let ((re (concat "\\[\\[\\(\\(file:\\)\\|\\([./~]\\)\\)\\([^]\n]+?"
                            (substring (org-image-file-name-regexp) 0 -2)
                            "\\)\\]" (if include-linked "" "\\]")))
                old file ov img)
            (while (re-search-forward re end t)
              (setq old (get-char-property-and-overlay (match-beginning 1)
                                                       'org-image-overlay)
                    file (expand-file-name
                          (concat (or (match-string 3) "") (match-string 4))))
              (when (file-exists-p file)
                (let ((file-thumb (format "%s%s_thumb.png" (file-name-directory file) (file-name-base file))))
                  (if (file-exists-p file-thumb)
                      (let ((thumb-time (nth 5 (file-attributes file-thumb 'string)))
                            (file-time (nth 5 (file-attributes file 'string))))
                        (if (time-less-p thumb-time file-time)
                            (shell-command (format org-imagemagick-display-command
                                                   file org-image-actual-width org-image-actual-width file-thumb) nil nil)))
                    (shell-command (format org-imagemagick-display-command
                                           file org-image-actual-width org-image-actual-width file-thumb) nil nil))
                  (if (and (car-safe old) refresh)
                      (image-refresh (overlay-get (cdr old) 'display))
                    (setq img (save-match-data (create-image file-thumb)))
                    (when img
                      (setq ov (make-overlay (match-beginning 0) (match-end 0)))
                      (overlay-put ov 'display img)
                      (overlay-put ov 'face 'default)
                      (overlay-put ov 'org-image-overlay t)
                      (overlay-put ov 'modification-hooks
                                   (list 'org-display-inline-remove-overlay))
                      (push ov org-inline-image-overlays))))))))))

;;;;;; Global Links

    ;;https://orgmode.org/manual/Handling-links.html
    ;;a globally unique ID property will be created and/or used to construct a
    ;;link
    (add-to-list 'org-modules 'org-id)
    (setq org-id-link-to-org-use-id t)

;;;;;; Latex

    ;;Use the ignore tag on headlines you'd like to have ignored (while not
    ;;ignoring their content)
    ;;https://emacs.stackexchange.com/questions/9492/is-it-possible-to-export-content-of-subtrees-without-their-headings#17677
    (require 'ox-extra)
    (ox-extras-activate '(ignore-headlines))
    ;; latex exporter
    (require 'ox-latex)
    ;;koma letter exporter
    (require 'ox-koma-letter)

    ;;ox-latex export classes
    (add-to-list 'org-latex-classes
                 ;;beamer class
                 ;;https://orgmode.org/worg/exporters/beamer/ox-beamer.html
                 '("beamer"
                   "\\documentclass\[presentation\]\{beamer\}"
                   ("\\section\{%s\}" . "\\section*\{%s\}")
                   ("\\subsection\{%s\}" . "\\subsection*\{%s\}")
                   ("\\subsubsection\{%s\}" . "\\subsubsection*\{%s\}")))

    ;;captions below on org table, for some reason above results in formatting errors
    ;;https://stackoverflow.com/questions/15642388/make-org-mode-table-caption-appear-below-table-when-exported-to-latex
    (setq org-latex-caption-above nil)

    ;;org-latex export process
    ;;http://orgmode.org/worg/exporters/anno-bib-template-worg.html
    (setq org-latex-pdf-process
          '("pdflatex -interaction nonstopmode -output-directory %o %f"
            "bibtex %b"
            "pdflatex -interaction nonstopmode -output-directory %o %f"))
    ;;doesn't seem to work, from bibtex layer documentation
    (setq org-ref-open-pdf-function
          (lambda (fpath)
            (start-process "zathura" "*helm-bibtex-zathura*" "/usr/bin/zathura" fpath)))

    ;;thesis latex class
    ;;https://github.com/robstewart57/phd-thesis
    (add-to-list 'org-latex-classes
                 '("thesis"
                   "\\documentclass{thesis}"
                   ("\\chapter{%s}" . "\\chapter*{%s}")
                   ("\\section{%s}" . "\\section*{%s}")
                   ("\\subsection{%s}" . "\\subsection*{%s}")
                   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                   ("\\paragraph{%s}" . "\\paragraph*{%s}")
                   ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

    ;;verbatim code wrap with listings in latex code blocks on export
    ;;https://emacs.stackexchange.com/questions/33010/how-to-word-wrap-within-code-blocks
    (add-to-list 'org-latex-packages-alist '("" "listings" nil))
    (setq org-latex-listings t)
    (setq org-latex-listings-options '(("breaklines" "true")))

;;;;;; Capture Templates

;;https://orgmode.org/manual/Capture-templates.html#Capture-templates
;; from Org Mode - Organize Your Life In Plain Text!

;; Use C-c c to start capture mode
    (global-set-key (kbd "C-c c") 'org-capture)

;; Capture templates for: TODO tasks, Notes, appointments, phone calls, meetings, and org-protocol
    (setq org-capture-templates
          (quote (("t" "Todo" entry (file "~/Notes/Refile.org")
                   "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
                  ("r" "Reading List" entry (file "~/Notes/Refile.org")
                   "* READING %?\n%U\n%a\n" :clock-in t :clock-resume t)
                  ("n" "Note" entry (file "~/Notes/Refile.org")
                   "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
                  ("j" "Journal" entry (file+datetree "~/Notes/近年.org")
                   "* %?\n%U\n" :clock-in t :clock-resume t :empty-lines 1)
                  ("w" "org-protocol" entry (file "~/Notes/Refile.org")
                   "* TODO Review %c\n%U\n" :immediate-finish t)
                  ("m" "Meeting" entry (file "~/Notes/Refile.org")
                   "* MEETING About %? :MEETING:\n%U" :clock-in t :clock-resume t)
                  ("p" "Phone call" entry (file "~/Notes/Refile.org")
                   "* PHONE %? :PHONE:\n%U" :clock-in t :clock-resume t)
                  ("h" "Habit" entry (file "~/Notes/Refile.org")
                   "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"%<<%Y-%m-%d %a .+1d/3d>>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n"))))


;;;;;; Structure Templates

    ;;structure templates
    ;;template for information outline trees in basics and research
    ;;https://blog.aaronbieber.com/2016/11/23/creating-org-mode-structure-templates.html

    ;; (add-to-list 'org-structure-template-alist
    ;;              '("it" (concat "- Header\n"
    ;;                             "  - Overview:\n"
    ;;                             "    -\n"
    ;;                             "  - Resources:\n"
    ;;                             "    -"))
    ;;              '("fig" (concat "# Figure\n"
    ;;                              "#+CAPTION:    Example Figure\n"
    ;;                              "#+LABEL:      fig:Example-1\n"
    ;;                              "#+ATTR_LaTeX: :width 90mm :float nil\n"
    ;;                              "[[./"))
    ;;              )

;;;;;; Refile

    ;; straight from Organize your life in plain text but, currently using helm
    ;; in spacemancs, so ido is not configured as he does

    ;; Targets include this file and any file contributing to the agenda - up to
    ;; 9 levels deep
    (setq org-refile-targets (quote ((nil :maxlevel . 9)
                                     (org-agenda-files :maxlevel . 9))))

    ;; Use full outline paths for refile targets
    (setq org-refile-use-outline-path t)

    ;; ;; Targets complete directly with IDO
    ;; (setq org-outline-path-complete-in-steps nil)

    ;; Allow refile to create parent tasks with confirmation
    (setq org-refile-allow-creating-parent-nodes (quote confirm))

    ;; Use the current window for indirect buffer display
    (setq org-indirect-buffer-display 'current-window)

    ;; ;; Refile settings
    ;; ;; Exclude DONE state tasks from refile targets
    ;; (defun bh/verify-refile-target ()
    ;;   "Exclude todo keywords with a done state from refile targets"
    ;;   (not (member (nth 2 (org-heading-components)) org-done-keywords)))

    ;; (setq org-refile-target-verify-function 'bh/verify-refile-target)

;;;;;; Global Tags

    ;;Set character number to align tag to
    (setq org-tags-column 40)
    ;;Tag list for org files
    (setq org-tag-alist '(
                          ;;time budget tags
                          ;;in development
                          ("Work" . ?W)
                          ("Projects" . ?P)
                          ("Family" . ?F)
                          ("Friends". ?f)
                          ("爱情" . ?A)
                          ))

;;;;;; PlantUML

    ;;define plantuml.jar path
    (setq org-plantuml-jar-path "/opt/plantuml/plantuml.jar")

;;;;;; Taskjuggler for Org-mode

    ;;manage projects with taskjuggler
    ;;https://orgmode.org/worg/exporters/taskjuggler/ox-taskjuggler.html
    ;;https://orgmode.org/worg/org-tutorials/org-taskjuggler.html#fn.1
    (use-package ox-taskjuggler)

    ;;use todo dependencies, required for using taskjuggler effectively
    ;;forces you to mark all child tasks as “DONE” before you can mark the parent as “DONE.”
    ;;https://orgmode.org/manual/TODO-dependencies.html#TODO-dependencies
    (setq org-enforce-todo-dependencies t)

    ;;sets task-juggler default project length
    ;;https://hugoideler.com/2018/09/org-mode-and-wide-taskjuggler-html-export/
    (setq org-taskjuggler-default-project-duration 999)

;;;;;; Clocking & Clock Reports

    ;;format clocksum lines using show cumulative hours rather than summing to
    ;;days after 24.0
    ;;https://stackoverflow.com/questions/17929979/emacs-org-mode-how-to-stop-total-in-column-view-showing-number-of-days
    (setq org-duration-format 'h:mm)

    ;; TODO punch-in/out system for parent task clocking on completion or clock
    ;; out when punched-in based on Organize your Life in Plain Text

    ;; Remove empty LOGBOOK drawers on clock out
    (defun remove-empty-drawer-on-clock-out ()
      (interactive)
      (save-excursion
        (beginning-of-line 0)
        (org-remove-empty-drawer-at "LOGBOOK" (point))))

    (add-hook 'org-clock-out-hook 'remove-empty-drawer-on-clock-out 'append)

;;;;;; Calendar & Agenda

    ;;required to include todos in calendar, and store the calendar id on
    ;;multiple exports
    (setq org-icalendar-include-todo t)
    (setq org-icalendar-store-UID t)

    ;;activate diary for anniversaries in agenda
    ;;https://orgmode.org/manual/Weekly_002fdaily-agenda.html
    (setq org-agenda-include-diary t)

    ;; Do not dim blocked tasks
    (setq org-agenda-dim-blocked-tasks nil)

    ;; Compact the block agenda view
    (setq org-agenda-compact-blocks t)

    ;; Custom agenda command definitions
    (setq org-agenda-custom-commands
          (quote (("N" "Notes" tags "NOTE"
                   ((org-agenda-overriding-header "Notes")
                    (org-tags-match-list-sublevels t)))
                  ("H" "Habits" tags-todo "STYLE=\"habit\""
                   ((org-agenda-overriding-header "Habits")
                    (org-agenda-sorting-strategy
                     '(todo-state-down effort-up category-keep))))
                  ("A" "Agenda"
                   ((agenda "" nil)
                    (tags "REFILE"
                          ((org-agenda-overriding-header "Tasks to Refile")
                           (org-tags-match-list-sublevels nil)))
                    (tags-todo "-CANCELLED/!"
                               ((org-agenda-overriding-header "Stuck Projects")
                                (org-agenda-skip-function 'bh/skip-non-stuck-projects)
                                (org-agenda-sorting-strategy
                                 '(category-keep))))
                    (tags-todo "-HOLD-CANCELLED/!"
                               ((org-agenda-overriding-header "Projects")
                                (org-agenda-skip-function 'bh/skip-non-projects)
                                (org-tags-match-list-sublevels 'indented)
                                (org-agenda-sorting-strategy
                                 '(category-keep))))
                    (tags-todo "-CANCELLED/!NEXT"
                               ((org-agenda-overriding-header (concat "Project Next Tasks"
                                                                      (if bh/hide-scheduled-and-waiting-next-tasks
                                                                          ""
                                                                        " (including WAITING and SCHEDULED tasks)")))
                                (org-agenda-skip-function 'bh/skip-projects-and-habits-and-single-tasks)
                                (org-tags-match-list-sublevels t)
                                (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-sorting-strategy
                                 '(todo-state-down effort-up category-keep))))
                    (tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
                               ((org-agenda-overriding-header (concat "Project Subtasks"
                                                                      (if bh/hide-scheduled-and-waiting-next-tasks
                                                                          ""
                                                                        " (including WAITING and SCHEDULED tasks)")))
                                (org-agenda-skip-function 'bh/skip-non-project-tasks)
                                (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-sorting-strategy
                                 '(category-keep))))
                    (tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
                               ((org-agenda-overriding-header (concat "Standalone Tasks"
                                                                      (if bh/hide-scheduled-and-waiting-next-tasks
                                                                          ""
                                                                        " (including WAITING and SCHEDULED tasks)")))
                                (org-agenda-skip-function 'bh/skip-project-tasks)
                                (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-sorting-strategy
                                 '(category-keep))))
                    (tags-todo "-CANCELLED+WAITING|HOLD/!"
                               ((org-agenda-overriding-header (concat "Waiting and Postponed Tasks"
                                                                      (if bh/hide-scheduled-and-waiting-next-tasks
                                                                          ""
                                                                        " (including WAITING and SCHEDULED tasks)")))
                                (org-agenda-skip-function 'bh/skip-non-tasks)
                                (org-tags-match-list-sublevels nil)
                                (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)))
                    (tags "-REFILE/"
                          ((org-agenda-overriding-header "Tasks to Archive")
                           (org-agenda-skip-function 'bh/skip-non-archivable-tasks)
                           (org-tags-match-list-sublevels nil))))
                   nil))))


;;;;;; Super-Agenda Views

    ;;agenda views will be developed based on utility theory as a method for
    ;;prioritizing tasks and separated by context

    ;; Org Super Agenda
    ;;https://github.com/alphapapa/org-super-agenda
    (use-package org-super-agenda
      :config
      (org-super-agenda-mode t))

    ;; On org-mode startup, activate org-super-agenda-mode
    (add-hook 'org-mode-hook 'org-super-agenda-mode)

    ;; (add-to-list 'org-agenda-custom-commands
    ;;              '("d" "Daily Agenda" agenda ""
    ;;                ((org-super-agenda-groups
    ;;                  '((:name "Daily Agenda"
    ;;                    :time-grid t
    ;;                    :todo "today"
    ;;                    :scheduled "today"
    ;;                    :discard
    ;;                    :anything))))))

    ;; (add-to-list 'org-agenda-custom-commands
    ;;              '("t" "All TODOs groups by category" alltodo ""
    ;;                ((org-super-agenda-groups '((:auto-category t))))
    ;;                ))

    ;; (setq org-agenda-custom-commands
    ;;       '(("d" "Daily View"
	  ;;          ((agenda "" ((org-super-agenda-groups
		;;                        '((:name "Daily Agenda"
		;; 	                              :time-grid t
    ;;                                 :todo "today"
    ;;                                 :discard)))))))))

            ;; ("d" "Daily View"
	          ;;  ((agenda "" ((org-super-agenda-groups
		        ;;                '((:name "Daily Agenda"
			      ;;                         :time-grid t
			      ;;                         :scheduled "today"
            ;;                         :deadline "today")
            ;;                  (:name "To-Do's"
			      ;;                         :deadline "today")))))))

            ;; ("v" "Utility View"
	          ;;  ((todo "" ((org-super-agenda-groups
		        ;;              '((:name "Utility IV"
            ;;                       :and (:priority>= "B" :deadline today))
		        ;;                (:name "Utility III"
            ;;                       :priority>= "B")
		        ;;                (:name "Utility I & II"
            ;;                       :anything
            ;;                       :discard)))))))

            ;; ("b" "Reading List"
	          ;;  ((agenda "" ((org-super-agenda-groups
		        ;;                '((:name "Reading List"
			      ;;                         :tag "book" "article")))))))

;;;;;; Org-timeline

    ;;https://github.com/Fuco1/org-timeline
    (add-hook 'org-agenda-finalize-hook 'org-timeline-insert-timeline :append)

;;;;;; TODO Keywords & Behavior

    ;;http://doc.norang.ca/org-mode.html#TasksAndStates
    ;; if editing org-todo-keywords, them must execute org-mode-restart from emacs

    ;;http://doc.norang.ca/org-mode.html#FastTodoSelection
    ;;press C-c C-t followed by the selection key
    (setq org-use-fast-todo-selection t)

    ;;insert an annotation in a task when it is marked as done including a
    ;;timestamp of when exactly that happened when triggering one of the DONE
    ;;states
    (setq org-log-done (quote time))
    ;;insert annotations when you change the deadline of a task, which will note
    ;;the previous deadline date and when it was changed
    (setq org-log-redeadline (quote time))
    ;;insert annotations when you change the scheduled date of a task, which
    ;;will note the previous scheduled date and when it was changed
    (setq org-log-reschedule (quote time))

    ;;sequence is one "flow" from open to closed todos
    ;;add @ for a note with timestamp and ! for a timestamp
    (setq org-todo-keywords
          (quote ((sequence "TODO(t@)" "NEXT(n@)" "|" "DONE(d@)" "DELEGATED(D@)")
                  (sequence "WAITING(w@)" "HOLD(h@)" "|" "CANCELLED(c@)" "PHONE(p@)" "MEETING(m@)")
                  (sequence "READING(r@)" "|" "READ(R@)")
                  (sequence "PROJECT(P@)" "|" "COMPLETE(C@)")
                  (sequence "OBJECTIVE(O@)" "KEY-RESULT(K@)" "|" "ACHIEVED(A@)" "FAILED(F@)" "CANCELLED(c@)"))))

    (setq org-todo-keyword-faces
          (quote (("TODO" :foreground "dark red" :weight bold)
                  ("NEXT" :foreground "dark blue" :weight bold)
                  ("DONE" :foreground "dark green" :weight bold)
                  ("WAITING" :foreground "orange" :weight bold)
                  ("HOLD" :foreground "orange" :weight bold)
                  ("READING" :foreground "dark red" :weight bold)
                  ("READ" :foreground "dark green" :weight bold)
                  ("CANCELLED" :foreground "forest green" :weight bold)
                  ("DELEGATED" :foreground "forest green" :weight bold)
                  ("MEETING" :foreground "medium blue" :weight bold)
                  ("PHONE" :foreground "medium blue" :weight bold)
                  ("PROJECT" :foreground "dark orange" :weight bold)
                  ("COMPLETE" :foreground "dark green" :weight bold)
                  ("OBJECTIVE" :foreground "orange" :weight bold)
                  ("KEY-RESULT" :foreground "orange" :weight bold)
                  ("ACHIEVED" :foreground "dark green" :weight bold)
                  ("FAILED" :foreground "medium purple" :weight bold))))

    ;; The tags are used to filter tasks in the agenda views
    (setq org-todo-state-tags-triggers
          (quote (("CANCELLED" ("CANCELLED" . t))
                  ("WAITING" ("WAITING" . t))
                  ("HOLD" ("WAITING") ("HOLD" . t))
                  (done ("WAITING") ("HOLD"))
                  ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
                  ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
                  ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))

;;;;;; Time Budgets

    ;; https://github.com/leoc/org-time-budgets
    ;; define categories for time budgets
    (require 'org-time-budgets)
    (setq org-time-budgets '((:title "Work" :tags "Work" :budget "30:00" :block workweek)
                             (:title "Projects" :tags "Projects" :budget "14:00" :block week)
                             (:title "Family" :tags "Family" :budget "2:55" :block week)
                             (:title "Friends" :tags "Friends" :budget "5:15" :block week)
                             (:title "爱情" :tags "爱情" :budget "5:15" :block week)))

;;;;;; Habits

    ;; enable org-habits
    ;; https://orgmode.org/manual/Tracking-your-habits.html
    ;; https://orgmode.org/worg/org-tutorials/tracking-habits.html
    (add-to-list 'org-modules 'org-habit)

;;;;;; Chef
    (use-package org-chef
      :ensure t)
;;;;;; Org Eval-after-load Tail
    )

;;;;; Outshine

  ;;spacemacs config & examples
  ;;https://github.com/syl20bnr/spacemacs/issues/5258
  ;;http://www.modernemacs.com/post/outline-ivy/
  ;;https://github.com/ekaschalk/.spacemacs.d
  ;;enable keybindings

  (use-package outshine
    :config
    (defvar outline-minor-mode-prefix "\M-#")
    ;;then outshine mode is active during org-mode
    (add-hook 'prog-mode-hook 'outshine-mode)
    (add-hook 'emacs-lisp-mode-hook 'outshine-mode)
    (add-hook 'LaTeX-mode-hook 'outshine-mode)
    (add-hook 'ess-mode-hook 'outshine-mode)
    (add-hook 'R-mode-hook 'outshine-mode)
    (add-hook 'conf-mode-hook 'outshine-mode)
    (add-hook 'conf-unix-mode-hook 'outshine-mode)
    ;;narrowing works within the headline rather than requiring to be on it
    (advice-add 'outshine-narrow-to-subtree :before
                (lambda (&rest args) (unless (outline-on-heading-p t)
                                       (outline-previous-visible-heading 1)))))
;;;;; Calfw

  ;; configure calfw for org-mode calendar

  ;; https://github.com/dudelson/spacemacs-calendar-layer
  ;; packages.el --- Calendar Framework Layer config for Spacemacs
  ;;
  ;; Copyright (c) 2017 sineer
  ;;
  ;; Author: Jérémie Plante <j@war.io>
  ;; URL: https://github.com/sineer/spacemacs-calendar-layer
  ;;
  ;; License: MIT

  (require 'calfw-cal)
  (require 'calfw-org)

  (defconst calendar-packages
    '(calfw
      calfw-org
      org-gcal
      alert))

  (defun calendar/init-calfw ()
    "Initialize calfw and add key-bindings"
    (use-package calfw
      :defer t
      :commands (cfw:open-calendar-buffer)
      :init
      (spacemacs/set-leader-keys "aC" 'cfw:open-calendar-buffer)
      :config
      (progn
        (define-key cfw:calendar-mode-map (kbd "SPC") 'spacemacs-cmds)
        (define-key cfw:calendar-mode-map (kbd "TAB") 'cfw:show-details-command)
        (define-key cfw:calendar-mode-map (kbd "C-j") 'cfw:navi-next-item-command)
        (define-key cfw:calendar-mode-map (kbd "C-k") 'cfw:navi-prev-item-command))))

  (defun calendar/init-calfw-org ()
    "Initialize calfw-org and add key-bindings"
    (use-package calfw-org
      :defer t
      :commands (cfw:open-org-calendar)
      :init
      (spacemacs/set-leader-keys "aoC" 'cfw:open-org-calendar)
      :config
      (progn
        (define-key cfw:org-schedule-map (kbd "SPC") 'spacemacs-cmds)
        (define-key cfw:org-schedule-map (kbd "TAB") 'cfw:org-open-agenda-day)
        (define-key cfw:org-custom-map (kbd "SPC") 'spacemacs-cmds)
        (define-key cfw:org-custom-map (kbd "TAB") 'cfw:org-open-agenda-day))))

  (defun calendar/init-org-gcal ()
    "Initialize org-gcal"
    (use-package org-gcal
      :defer t
      :commands (org-gcal-sync
                 org-gcal-fetch
                 org-gcal-post-at-point
                 org-gcal-delete-at-point)))

  (defun calendar/init-alert ()
    "Initialize alert"
    (use-package alert
      :defer t))

  (defun my-open-calendar ()
    (interactive)
    (cfw:open-calendar-buffer
     :contents-sources
     (list
      (cfw:org-create-source "Blue")  ;;orgmode source
      (cfw:cal-create-source "Orange"))))  ;;diary source

;;;;; PDF Tools

  ;;https://babbagefiles.blogspot.no/2017/11/more-pdf-tools-tricks.html
  ;;https://www.reddit.com/r/emacs/comments/6905nl/pdf_tools_dark_theme/
  ;;open dark colors on pdf open
  (add-hook 'pdf-tools-enabled-hook 'pdf-view-midnight-minor-mode)

  ;;set pdf dark colors (foreground, background), currently spacemacs dark colors
  (setq pdf-view-midnight-colors '("#b2b2b2" ."#292b2e"))

  ;;http://pragmaticemacs.com/emacs/even-more-pdf-tools-tweaks/
  ;;set default display view
  (setq-default pdf-view-display-size 'fit-page)
  ;;automatically annotate highlights
  (setq pdf-annot-activate-created-annotations t)
  ;;save after adding comment
  (advice-add 'pdf-annot-edit-contents-commit :after 'bjm/save-buffer-no-args)

;;;;; Olivetti

;;set body width of olivetti to 81, for some reason 80 leads to broken wrap despite org width being 80
  (setq olivetti-body-width 81)

;;;;; Neotree

  ;;turn on all-the-icons package
  ;;for arch, install aur package, then run fc-cache -f -v
  ;;https://aur.archlinux.org/packages/emacs-all-the-icons/
  (use-package all-the-icons)
  ;;set neotree theme to all the icons
  ;;https://github.com/domtronn/all-the-icons.el/wiki/Neotree
  ;;https://github.com/jaypei/emacs-neotree
  (setq neo-theme 'icons)

;;;;; Colors Layer

  (setq-default
   dotspacemacs-configuration-layers '((colors
                                        :variables
                                        colors-colorize-identifiers
                                        'all)))

;;;;; mu4e Email

  ;;https://gist.github.com/areina/3879626
  (require 'mu4e)

  (setq-default dotspacemacs-configuration-layers
                '((mu4e :variables
                        mu4e-installation-path "~/.emacs.d/layers/+email/")))

  ;; default
  (setq mu4e-maildir (expand-file-name "~/Mail")
        mu4e-drafts-folder "/[Gmail].Drafts"
        mu4e-sent-folder   "/[Gmail].Sent Mail"
        mu4e-trash-folder  "/[Gmail].Trash"
        mu4e-update-interval nil
        mu4e-compose-signature-auto-include nil
        mu4e-view-show-images t
        mu4e-view-show-addresses t)

  ;; don't save message to Sent Messages, GMail/IMAP will take care of this
  (setq mu4e-sent-messages-behavior 'delete)

  ;; setup some handy shortcuts
  (setq mu4e-maildir-shortcuts
        '(("/INBOX"             . ?i)
          ("/[Gmail].Sent Mail" . ?s)
          ("/[Gmail].Trash"     . ?t)))

  ;; allow for updating mail using 'U' in the main view:
  (setq mu4e-get-mail-command "offlineimap -o")

  ;; something about ourselves
  ;; I don't use a signature...
  (setq
   user-mail-address "alecceckert@gmail.com"
   user-full-name  "Alec Eckert"
   ;; message-signature
   ;;  (concat
   ;;    "Foo X. Bar\n"
   ;;    "http://www.example.com\n")
   )

  ;; sending mail -- replace USERNAME with your gmail username
  ;; also, make sure the gnutls command line utils are installed
  ;; package 'gnutls-bin' in Debian/Ubuntu, 'gnutls' in Archlinux.

  (require 'smtpmail)

  (setq message-send-mail-function 'smtpmail-send-it
        starttls-use-gnutls t
        smtpmail-starttls-credentials
        '(("smtp.gmail.com" 587 nil nil))
        smtpmail-auth-credentials
        (expand-file-name "~/.authinfo.gpg")
        smtpmail-default-smtp-server "smtp.gmail.com"
        smtpmail-smtp-server "smtp.gmail.com"
        smtpmail-smtp-service 587
        smtpmail-debug-info t)
;;;;; Tail

  )

;;; Custom Variables

;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("75d3dde259ce79660bac8e9e237b55674b910b470f313cdf4b019230d01a982a" "f0dc4ddca147f3c7b1c7397141b888562a48d9888f1595d69572db73be99a024" "26d49386a2036df7ccbe802a06a759031e4455f07bda559dcf221f53e8850e69" "100e7c5956d7bb3fd0eebff57fde6de8f3b9fafa056a2519f169f85199cc1c96" "6d589ac0e52375d311afaa745205abb6ccb3b21f6ba037104d71111e7e76a3fc" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" "93a0885d5f46d2aeac12bf6be1754faa7d5e28b27926b8aa812840fe7d0b7983" "a3fa4abaf08cc169b61dea8f6df1bbe4123ec1d2afeb01c17e11fdc31fc66379" "4639288d273cbd3dc880992e6032f9c817f17c4a91f00f3872009a099f5b3f84" "86704574d397606ee1433af037c46611fb0a2787e8b6fd1d6c96361575be72d2" default)))
 '(evil-want-Y-yank-to-eol nil)
 '(package-selected-packages
   (quote
    (lv toml-mode racer flycheck-rust cargo rust-mode vimrc-mode dactyl-mode org-timeline org-sticky-header org-chef auctex-latexmk typit powerline sass-mode pyenv-mode spinner pacmacs ox-pandoc outshine orgit org-time-budgets org-ref pdf-tools key-chord org-pomodoro mu4e-alert alert log4e markdown-toc magit-gitflow magit-popup hydra hy-mode parent-mode projectile helm-gitignore helm-c-yasnippet helm-bibtex parsebib graphviz-dot-mode git-gutter-fringe+ git-gutter-fringe fringe-helper git-gutter+ flyspell-correct-helm flycheck-pos-tip pos-tip pkg-info epl flx find-by-pinyin-dired highlight evil-magit transient git-commit smartparens anzu evil goto-chg undo-tree ess-smart-equals ess-R-data-view julia-mode company-web web-completion-data company-statistics company-shell company-auctex company-anaconda bind-map bind-key biblio auto-yasnippet packed anaconda-mode f s all-the-icons ace-pinyin avy helm-core async ac-ispell popup evil-goggles pyim org-super-agenda helm-company doom-themes iedit ivy helm magit dash yapfify yaml-mode xterm-color xresources-theme xkcd web-mode unfill mmt tagedit sudoku smeargle slim-mode shell-pop scss-mode rainbow-mode rainbow-identifiers pyvenv pytest pyim-basedict py-isort pug-mode plantuml-mode pip-requirements pangu-spacing pandoc-mode outorg ht tablist org-present org-noter org-mime org-download org-dashboard org-beautify-theme olivetti mwim multi-term mu4e-maildirs-extension gntp moe-theme mmm-mode minimap markdown-mode lua-mode live-py-mode insert-shebang dash-functional htmlize helm-pydoc helm-css-scss haml-mode gnuplot gitignore-mode gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link git-gutter gh-md fuzzy flyspell-correct flycheck fish-mode with-editor ctable ess eshell-z eshell-prompt-extras esh-help emmet-mode diff-hl deft cython-mode csv-mode company color-identifiers-mode circadian calfw-org calfw-cal calfw biblio-core beacon yasnippet auto-dictionary auctex pythonic memoize pinyinlib auto-complete 2048-game exec-path-from-shell evil-ediff ws-butler winum which-key volatile-highlights vi-tilde-fringe uuidgen use-package toc-org spaceline restart-emacs request rainbow-delimiters popwin persp-mode pcre2el paradox org-plus-contrib org-bullets open-junk-file neotree move-text macrostep lorem-ipsum linum-relative link-hint indent-guide hungry-delete hl-todo highlight-parentheses highlight-numbers highlight-indentation helm-themes helm-swoop helm-projectile helm-mode-manager helm-make helm-flx helm-descbinds helm-ag google-translate golden-ratio flx-ido fill-column-indicator fancy-battery eyebrowse expand-region evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-surround evil-search-highlight-persist evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-lisp-state evil-indent-plus evil-iedit-state evil-exchange evil-escape evil-args evil-anzu eval-sexp-fu elisp-slime-nav dumb-jump diminish define-word column-enforce-mode clean-aindent-mode auto-highlight-symbol auto-compile aggressive-indent adaptive-wrap ace-window ace-link ace-jump-helm-line))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(evil-goggles-change-face ((t (:inherit diff-removed))))
 '(evil-goggles-delete-face ((t (:inherit diff-removed))))
 '(evil-goggles-paste-face ((t (:inherit diff-added))))
 '(evil-goggles-undo-redo-add-face ((t (:inherit diff-added))))
 '(evil-goggles-undo-redo-change-face ((t (:inherit diff-changed))))
 '(evil-goggles-undo-redo-remove-face ((t (:inherit diff-removed))))
 '(evil-goggles-yank-face ((t (:inherit diff-changed)))))
