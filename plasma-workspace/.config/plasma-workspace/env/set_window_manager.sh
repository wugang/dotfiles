# Deisable KWinand use i3gaps as WM
export KDEWM=/usr/bin/i3

# Compositor (Animations, Shadows, Transparency
# xcompmgr -C
compton -cCFB --backend glx --vsync opengl
