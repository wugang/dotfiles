#!/usr/bin/env sh
#
# Description: launch polybar, activated in i3 config on startup


# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch bar1
polybar bar1

echo "Bars launched..."
