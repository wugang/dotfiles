#!/bin/sh
#
# Desctiption: indicates in polybar if bluetooth is active; if active shows , else nothing
# Resources:
# https://github.com/x70b1/polybar-scripts/tree/master/polybar-scripts/isactive-bluetooth

icon=""

if [ "$(bluetooth)" = "bluetooth = on" ]; then
	  echo $icon
else
	  echo ""
fi
