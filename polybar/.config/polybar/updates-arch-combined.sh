#!/bin/sh
#
# Description:
# Displays updates for aur and community packages on polybar
# Dependencies: pacman-contrib for checkupdates script and command
# Resources:
# https://github.com/x70b1/polybar-scripts/blob/master/polybar-scripts/updates-arch-combined/updates-arch-combined.sh

icon=""

updates_arch=$(checkupdates | wc -l)

if ! updates_aur=$(trizen -Su --aur --quiet | wc -l); then
    updates_aur=0
fi

#add aur and community together
updates=$(("$updates_arch" + "$updates_aur"))

#replace aur/arch with $updates if want 1 number
if [ "$updates" -gt 0 ]; then
    echo "$updates_aur $icon $updates_arch"
else
    echo " "
fi
