"----- vimrc ----

" Vundle Config

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Keep Plugin commands between vundle#begin/end.

" Zim Wiki Syntax - Vim
Plugin 'joanrivera/vim-zimwiki-syntax'

" Syntastic Extention - checks syntax
Plugin 'scrooloose/syntastic'

" Add PEP8 Checking
Plugin 'nvie/vim-flake8'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

"---- Settings ----

"execute pathogen
execute pathogen#infect()

"PEP8 Indentation for Python
"au BufNewFile,BufRead *.py
"    \ set tabstop=4
"    \ set softtabstop=4
"    \ set shiftwidth=4
"    \ set textwidth=79
"    \ set expandtab
"    \ set autoindent
"    \ set fileformat=unix

"Indentation for .js .html. css filetypes
"au BufNewFile,BufRead *.js, *.html, *.css
"    \ set tabstop=2
"    \ set softtabstop=2
"    \ set shiftwidth=2

"set colorscheme
"colorscheme solarized

"background color
"set background=dark

"syntax highlighting
syntax on

"indent for filetypes
filetyp plugin indent on

"----- NERDTree -----

"start NERDtree
autocmd vimenter * NERDTree
"& if no file is secified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
"NERDTree Toggle
map <C-n> :NERDTreeToggle<CR>
"close vim when only NERDTree is open
"autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
"NERDTree arrows
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

"show line numbers
set number

"line number color
"hi LineNr	ctermfg=Grey

"width of document
set tw=79

"auto wrap on load
"set wrap

"wrap at a character in break at option
set linebreak

"display the current mode
set showmode

"set color column color
"highlight ColorColumn ctermbg=gray

"color column at 80 characters
set colorcolumn=80

"don't beep
set visualbell

"don't beep
set noerrorbells

"set encoding
set encoding=utf-8

"highlight current line
"set cursorline

"visual autocomplete
set wildmenu

"highlight matching [{()}]
set showmatch

"python highlighting
let python_highlight_all=1

"---- Keybinds ----

"split vim  navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

