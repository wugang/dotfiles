" ==VimperatorColorSchema==
" name: Spacemacs
" edited from the following source
" url: https://github.com/suzuken/dotfiles/blob/master/vimperator/colors/solarized.vimp
" ==/VimperatorColorSchema==
"
" based on:
" https://github.com/nashamri/spacemacs-theme

" SOLARIZED HEX     16/8 TERMCOL  XTERM/HEX   L*A*B      RGB         HSB
" --------- ------- ---- -------  ----------- ---------- ----------- -----------
" base03    #002b36  8/4 brblack  234 #1c1c1c 15 -12 -12   0  43  54 193 100  21
" base02    #073642  0/4 black    235 #262626 20 -12 -12   7  54  66 192  90  26
" base01    #586e75 10/7 brgreen  240 #585858 45 -07 -07  88 110 117 194  25  46
" base00    #657b83 11/7 bryellow 241 #626262 50 -07 -07 101 123 131 195  23  51
" base0     #839496 12/6 brblue   244 #808080 60 -06 -03 131 148 150 186  13  59
" base1     #93a1a1 14/4 brcyan   245 #8a8a8a 65 -05 -02 147 161 161 180   9  63
" base2     #eee8d5  7/7 white    254 #e4e4e4 92 -00  10 238 232 213  44  11  93
" base3     #fdf6e3 15/7 brwhite  230 #ffffd7 97  00  10 253 246 227  44  10  99
" yellow    #b58900  3/3 yellow   136 #af8700 60  10  65 181 137   0  45 100  71
" orange    #cb4b16  9/3 brred    166 #d75f00 50  50  55 203  75  22  18  89  80
" red       #dc322f  1/1 red      160 #d70000 50  65  45 220  50  47   1  79  86
" magenta   #d33682  5/5 magenta  125 #af005f 50  65 -05 211  54 130 331  74  83
" violet    #6c71c4 13/5 brmagenta 61 #5f5faf 50  15 -45 108 113 196 237  45  77
" blue      #268bd2  4/4 blue      33 #0087ff 55 -10 -45  38 139 210 205  82  82
" cyan      #2aa198  6/6 cyan      37 #00afaf 60 -35 -05  42 161 152 175  74  63
" green     #859900  2/2 green     64 #5f8700 60 -20  65 133 153   0  68 100  60
"
" Spacemacs HEX     Color
" --------- ------- ------------------
" base00    #1f2022 background dark
" base01    #282828
" base02    #444155 foreground light
" base03    #585858
" base04    #b8b8b8
" base05    #a3a3a3 foreground dark
" base06    #e8e8e8
" base07    #f8f8f8 background light
" base08    #f2241f red1
" base09    #ffa500 yellow1
" base0A    #b1951d yellow2
" base0B    #67b11d green1
" base0C    #2d9574 green2
" base0D    #4f97d7 blue1
" base0E    #a31db1 purple
" base0F    #b03060 red2

hi Hint font-family: monospace; font-size: 15px; font-weight: normal; color: #dc322f; background-color: #282828; border-color: #b8b8b8; border-width: 0px; border-style: solid; padding: 0px 5px 0px 5px; text-transform: uppercase;
hi HintElem background-color: #282828; color: #a31db1;
hi HintActive background-color: #a31bd1; color: #282828;
hi -append Hint         background-color: rgba(253, 246, 227, 0.7); border: 1px solid #fdf6e3; -moz-border-radius: 4px; -moz-box-shadow: 0px 0px 1px black;
hi -append HintElem     text-decoration: none; -moz-border-radius: 4px; -moz-box-shadow: 0 0 2px #282828;
hi -append HintActive   text-decoration: none; -moz-border-radius: 4px; -moz-box-shadow: 0 0 2px #282828;

hi StatusLine     color: #93a1a1; background: #282828; font-weight: normal; font-size: 12pt;
" hi StatusLineBroken   color:  #93a1a1; background: #ffffd7; font-weight: normal; font-size: 12pt;
" hi StatusLineExtended color:  #93a1a1; background: #af005f; font-weight: normal; font-size: 12pt;
" hi StatusLineSecure   color:  #93a1a1; background: #b58900; font-weight: normal; font-size: 12pt;

hi Normal     color:  #b8b8b8;  background: #282828; font-size: 12pt; opacity: 0.9;
hi CmdLine    color:  #b8b8b8;  background: #282828; font-size: 14pt;
hi InfoMsg    color:  #b8b8b8;  background: #282828; font-size: 12pt;
hi ModeMsg    color:  #b8b8b8;  background: #282828; font-size: 12pt;
hi MoreMsg    color:  #b8b8b8; background: #282828;
hi LineNr   color:  #f2241f; background: #282828; font-size: 12pt;
hi Question   color: #ffa500;  background: #282828;  font-size: 12pt;
hi WarningMsg   color: #f2241f;  background: #282828;  font-size: 12pt;
hi ErrorMsg   color: #282828;  background: red;  font-size: 12pt;
hi NonText    background: #000;
hi Null     color: CornflowerBlue;
hi Tag      color: CornflowerBlue;
hi Number   color: CornflowerBlue;

hi CompTitle    background: #1c1c1c;
hi CompTitle>*  color: #f2241f; border-bottom: 1px solid #3c3c3f; padding: 1px 0.5ex;
hi CompItem[selected] color: cyan;  background: #333;
hi CompDesc   color: #b03060;

hi Title    color:  #282828;
hi Indicator    color:  #ffa500;
hi String   color:  #282828;
hi Number   color:  #282828;
hi Object   color: #2d9574;
hi Function   color:  #4f97d7;
hi URL    color:  #67b11d;

hi TabNumber  color: #282828; font-weight: bold; font-size:10pt;

style -name tab chrome://* <<EOM
  .tabbrowser-strip {
    background                         : #282828 !important;
  }
EOM
