;=====================================================
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;=====================================================

; * Colors
  
[colors]
;wpgtk color template
;pull colors from xresources generated from wpgtk
background = ${{xrdb:color8:{color8}}}
foreground = ${{xrdb:color15:{color15}}}
active = {active}
inactive = {inactive}
highlight = ${{xrdb:color2:{color2}}}
error = ${{xrdb:color9:{color9}}}

; * Bar 1

[bar/bar1]
; Use the following command to list available outputs:
; If unspecified, the application will pick the first one it finds.
; $ xrandr -q | grep " connected" | cut -d ' ' -f1
;monitor = ${{env:MONITOR:HDMI-1}}

; Use the specified monitor as a fallback if the main one is not found.
monitor-fallback =

; Require the monitor to be in connected state
; XRandR sometimes report my monitor as being disconnected (when in use)
monitor-strict = false

; Tell the Window Manager not to configure the window.
; Use this to detach the bar if your WM is locking its size/position.
;override-redirect = true

; Put the bar at the bottom of the screen
bottom = false

; Dimension defined as pixel value (e.g. 35) or percentage (e.g. 50%),
; the percentage can optionally be extended with a pixel offset like so:
; 50%:-10, this will result in a width or height of 50% minus 10 pixels
width = 100%
height = 30

; Offset defined as pixel value (e.g. 35) or percentage (e.g. 50%)
; the percentage can optionally be extended with a pixel offset like so:
; 50%:-10, this will result in an offset in the x or y direction 
; of 50% minus 10 pixels
offset-x = 0
offset-y = 0

; Value used for drawing rounded corners
; Individual top/bottom values can be defined using:
;   radius-{{top,bottom}}
radius = 0.0

; Prefer fixed center position for the `modules-center` block
; When false, the center position will be based on the size of the other
fixed-center = true

; Background argb color (e.g. #f00, #ff992a, #ddff1023)
; Foreground argb color (e.g. #f00, #ff992a, #ddff1023)
background = ${{colors.background}}
foreground = ${{colors.foreground}}

; Background gradient (vertical steps)
;   background-[0-9]+ = #aarrggbb
;background-0 = 

; Under-/overline pixel size and argb color
; Individual values can be defined using:
;   {{overline,underline}}-size
;   {{overline,underline}}-color
line-size = 0
line-color = #f00

; Values applied to all borders
; Individual side values can be defined using:
;   border-{{left,top,right,bottom}}-size
;   border-{{left,top,right,bottom}}-color
border-size = 0
border-color = #00000000

; Number of spaces to add at the beginning/end of the bar
; Individual side values can be defined using:
;   padding-{{left,right}}
padding-left = 2
padding-right = 2

; Number of spaces to add before/after each module
; Individual side values can be defined using:
;   module-margin-{{left,right}}
module-margin-left = 2
module-margin-right = 2

; Fonts are defined using <font-name>;<vertical-offset>
; Font names are specified using a fontconfig pattern.
;   font-0 = NotoSans-Regular:size=8;2
;   font-1 = MaterialIcons:size=10
;   font-2 = Termsynu:size=8;-1
;   font-3 = FontAwesome:size=10
; See the Fonts wiki page for more details
font-0 = "Nimbus Sans:size=14;1"
font-1 = "Noto Sans CJK TC:size=14;1"
font-2 = "MaterialIcons:size=12;0"
font-3 = "FontAwesome:size=12;0"

; Modules are added to one of the available blocks
;   modules-left = cpu ram
;   modules-center = xwindow xbacklight
;   modules-right = ipc clock
modules-left = i3 xwindow
modules-center = date
modules-right = updates filesystem bluetooth wlan eth battery0 battery1

; The separator will be inserted between the output of each module
separator =

; Position of the system tray window
; If empty or undefined, tray support will be disabled
; NOTE: A center aligned tray will cover center aligned modules
;
; Available positions:
;   left
;   center
;   right
tray-position = left

; Pad the sides of each tray icon
tray-padding = 1

; Scale factor for tray clients
tray-scale = 1.0

; Enable pseudo transparency
; Will automatically be enabled if a fully transparent
; background color is defined using `tray-background`
tray-transparent = false

; Background color for the tray container
; By default the tray container will use the bar
; background color.
; Note: 32 bit alpha blending is not supported.
tray-background = ${{colors.inactive}}

; If true, the bar will not shift its
; contents when the tray changes
tray-detached = false

; Tray icon max size
tray-maxsize = 16

; Restack the bar window and put it above the
; selected window manager's root
;
; Fixes the issue where the bar is being drawn
; on top of fullscreen window's
;
; Currently supported WM's:
;   bspwm
;   i3 (requires: `override-redirect = true`)
;wm-restack = i3

scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev

cursor-click = pointer
cursor-scroll = ns-resize

; Enable support for inter-process messaging
; See the Messaging wiki page for more details.
enable-ipc = true

; * Settings

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over

; * Global

[global/wm]
; Adjust the _NET_WM_STRUT_PARTIAL bottom value
;   Used for bottom aligned bars
margin-top = 0

; Adjust the _NET_WM_STRUT_PARTIAL top value
;   Used for top aligned bars
margin-bottom = 0

; vim:ft=dosini

; * Modules
; ** I3
  
[module/i3]
type = internal/i3
format = <label-state><label-mode>
index-sort = true
wrapping-scroll = false
fuzzy-matching = true

; Only show workspaces on the same output as the bar
pin-workspaces = true

; Workspace icons
ws-icon-0 = 1;一
ws-icon-1 = 2;二
ws-icon-2 = 3;三
ws-icon-3 = 4;四
ws-icon-4 = 5;五
ws-icon-5 = 6;六
ws-icon-6 = 7;七
ws-icon-7 = 8;八
ws-icon-8 = 9;九
ws-icon-9 = 10;十
ws-icon-default = 

label-mode-foreground = ${{colors.foreground}}
label-mode-background = ${{colors.highlight}}
label-mode-padding = 2

; focused = Active workspace on focused monitor
label-focused = %index%
label-focused-background = ${{colors.active}}
label-focused-underline = ${{colors.active}}
label-focused-padding = 2

; unfocused = Inactive workspace on any monitor
label-unfocused = %index%
label-unfocused-background = ${{colors.inactive}}
label-unfocused-padding = 2 

; visible = Active workspace on unfocused monitor
label-visible = %index%
label-visible-background = ${{colors.highlight}}
label-visible-padding = 2

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = ${{colors.highlight}}
label-urgent-padding = 2

; ** xWindow

[module/xwindow]
type = internal/xwindow
format-background = ${{colors.active}}
format-padding = 1
label = %title:0:30:...%

; ** Date

[module/date]
type = internal/date
interval = 5

date = %Y年 %m月 %d日
date-alt = %A, %d %B %Y

time =  %H:%M:%S 
time-alt = %H:%M

format-prefix =
format-prefix-foreground = ${{colors.foreground}}
format-underline = #0a6cf5
format-background = ${{colors.background}}

label = %date% %time%

; ** Filesystem

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

label-mounted = %mountpoint%%{{F-}}: %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${{colors.foreground}}

; ** Updates

[module/updates]
type = custom/script
exec = ~/.config/polybar/updates-arch-combined.sh
interval = 600

; ** Bluetooth

[module/bluetooth]
type = custom/script
exec = ~/.config/polybar/isactive-bluetooth.sh
interval = 10

; ** Wlan

[module/wlan]
type = internal/network
interface = wlp4s0
interval = 3.0

format-connected = <ramp-signal> <label-connected>
format-connected-underline = ${{colors.active}}
label-connected = %essid%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${{colors.active}}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${{colors.foreground}}

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-foreground = ${{colors.foreground-alt}}

; ** Eth

[module/eth]
type = internal/network
interface = enp0s31f6
interval = 3.0

format-connected-underline = ${{colors.active}}
format-connected-prefix = " "
format-connected-prefix-foreground = ${{colors.foreground}}
label-connected = %local_ip%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${{self.format-connected-underline}}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${{colors.foreground-alt}}

; ** Battery 0

[module/battery0]
;use the following command to list batteries and adapters;
;ls -1 /sys/class/power_supply/
type = internal/battery
battery = BAT0
adapter = AC
full-at = 98
poll-interval = 5

format-charging =  <animation-charging> <label-charging>
format-charging-underline = ${{colors.active}}
format-charging-padding = 1

format-discharging = <ramp-capacity> <label-discharging>
format-discharging-underline = ${{colors.active}}
format-discharging-padding = 1

;format-full-prefix = " "
format-full-prefix-foreground = ${{colors.foreground}}
format-full-underline = ${{colors.active}}
format-full = <label-full> <ramp-capacity>
format-full-background = ${{colors.background}}
format-full-padding = 1

ramp-capacity-0 = 
ramp-capacity-1 =  
ramp-capacity-2 =  
ramp-capacity-3 = 
ramp-capacity-4 =  
ramp-capacity-foreground = ${{colors.foreground}}

animation-charging-0 = 
animation-charging-1 =  
animation-charging-2 =  
animation-charging-3 = 
animation-charging-4 =  
animation-charging-foreground = ${{colors.foreground}}
;framerate in milliseconds
animation-charging-framerate = 2000

; ** Battery 1

[module/battery1]
type = internal/battery
battery = BAT1
adapter = AC
full-at = 98
poll-interval = 5

format-charging =  <animation-charging> <label-charging>
format-charging-underline = #ffb52a
format-charging-padding = 1

format-discharging = <ramp-capacity> <label-discharging>
format-discharging-underline = ${{colors.active}}
format-discharging-padding = 1

;format-full-prefix = " "
format-full-prefix-foreground = ${{colors.foreground}}
format-full-underline = ${{colors.active}}
format-full = <label-full> <ramp-capacity>
format-full-background = ${{colors.background}}
format-full-padding = 1

ramp-capacity-0 = 
ramp-capacity-1 =  
ramp-capacity-2 =  
ramp-capacity-3 = 
ramp-capacity-4 =  
ramp-capacity-foreground = ${{colors.foreground}}

animation-charging-0 = 
animation-charging-1 =  
animation-charging-2 =  
animation-charging-3 = 
animation-charging-4 =  
animation-charging-foreground = ${{colors.foreground}}
;framerate in milliseconds
animation-charging-framerate = 2000

; ** MPD

;Unused Modules
[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-play = ⏵
icon-pause = ⏸
icon-stop = ⏹
icon-prev = ⏮
icon-next = ⏭

label-song-maxlen = 25
label-song-ellipsis = true
format-playing-padding = 2
format-playing-background = ${{colors.highlight}}

; ** xBacklight

[module/xbacklight]
type = internal/xbacklight

format = <label> <bar>
label = BL

bar-width = 10
bar-indicator = |
bar-indicator-foreground = #ff
bar-indicator-font = 2
bar-fill = ─
bar-fill-font = 2
bar-fill-foreground = ${{colors.foreground}}
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = ${{colors.foreground}}

; ** Backlight-acpi

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

; ** CPU

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
format-prefix-foreground = ${{colors.foreground}}
format-underline = ${{colors.active}}
label = %percentage:2%%

; ** Memory


[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
format-prefix-foreground = ${{colors.foreground}}
format-underline = ${{colors.active}}
label = %percentage_used%%

; ** BSPWM 

; [module/bspwm]
; type = internal/bspwm

; label-focused = %index%
; label-focused-background = ${{colors.active}}
; label-focused-padding = 1

; label-occupied = %index%
; label-occupied-padding = 1

; label-urgent = %index%!
; label-urgent-background = ${{colors.active}}
; label-urgent-padding = 1

; label-empty = %index%
; label-empty-foreground = ${{colors.foreground}}
; label-empty-padding = 1


; ** Temperature

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <ramp> <label>
format-underline = ${{colors.active}}
format-warn = <ramp> <label-warn>
format-warn-underline = ${{colors.highlight}}

label = %temperature%
label-warn = %temperature%
label-warn-foreground = ${{colors.foreground}}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = ${{colors.foreground}}

; ** Power Menu

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = 
label-open-foreground = ${{colors.foreground}}
label-close =  cancel
label-close-foreground = ${{colors.foreground}}
label-separator = |
label-separator-foreground = ${{colors.foreground}}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = sudo reboot

menu-2-0 = power off
menu-2-0-exec = sudo poweroff
menu-2-1 = cancel
menu-2-1-exec = menu-open-0

; [module/volume]
; type = internal/volume

; format-volume = <label-volume> <bar-volume>
; label-volume = VOL
; label-volume-foreground = ${{colors.foreground}}

; format-muted-prefix = " "
; format-muted-foreground = ${{colors.foreground}}
; label-muted = sound muted

; bar-volume-width = 10
; bar-volume-foreground-0 = #55aa55
; bar-volume-foreground-1 = #55aa55
; bar-volume-foreground-2 = #55aa55
; bar-volume-foreground-3 = #55aa55
; bar-volume-foreground-4 = #55aa55
; bar-volume-foreground-5 = #f5a70a
; bar-volume-foreground-6 = #ff5555
; bar-volume-gradient = false
; bar-volume-indicator = |
; bar-volume-indicator-font = 2
; bar-volume-fill = ─
; bar-volume-fill-font = 2
; bar-volume-empty = ─
; bar-volume-empty-font = 2
; bar-volume-empty-foreground = ${{colors.foreground}}

; ** Xkeyboard
[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${{colors.foreground}}
format-prefix-underline = ${{colors.active}}

label-layout = %layout%
label-layout-underline = ${{colors.active}}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${{colors.background}}
label-indicator-underline = ${{colors.active}}