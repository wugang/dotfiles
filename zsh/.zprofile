# Guide to ZSH startup files
# http://zsh.sourceforge.net/Guide/zshguide02.html

#Used for executing user's commands at start, will be sourced when starting as a
#login shell.

#by default it contains one line which source the /etc/profile.
emulate sh -c 'source /etc/profile'

# Path to ruby gems on Arch
# https://superuser.com/questions/1041134/gem-installs-not-executing-after-update-system-wide-update-arch-linux
PATH="$(ruby -e 'print Gem.user_dir')/bin:$PATH"

# Set bin path
# https://stackoverflow.com/questions/9347478/how-to-edit-path-variable-in-zsh
path+=/usr/bin
path+=~/bin
path=($^path(N))

# set file manager for nnn
export NNN_DE_FILE_MANAGER="dolphin"
# set text editor for nnn
export NNN_USE_EDITOR="emacsclient -nc"

# set qt & gtk themes
export QT_QPA_PLATFORMTHEME=qt5ct 
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
#export QT_STYLE_OVERRIDE=adwaita

