# zshenv Used for setting system-wide environment variables; it should not
# contain commands that produce output or assume the shell is attached to a tty.
# This file will always be sourced, this cannot be overridden.
# https://wiki.archlinux.org/index.php/Zsh#Startup.2FShutdown_files

export QT_QPA_PLATFORMTHEME="qt5ct"
